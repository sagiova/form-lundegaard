drop schema if exists lundegaard cascade;

create schema lundegaard;

set search_path to lundegaard;

CREATE TABLE request_type
(
  type VARCHAR(30) PRIMARY KEY
);

INSERT INTO request_type (code) VALUES ('CONTRACT_ADJUSTMENT');
INSERT INTO request_type (code) VALUES ('DAMAGE_CASE');
INSERT INTO request_type (code) VALUES ('COMPLAINT');

create table form
(
    id                       SERIAL NOT NULL PRIMARY KEY,
    request_type             VARCHAR(30) NOT NULL,
    email                    VARCHAR(50) NOT NULL,
    policy_number            VARCHAR(50) NOT NULL,
    name                     VARCHAR(50) NOT NULL,
    surname                  VARCHAR(50) NOT NULL,
    text                     TEXT NOT NULL,

    FOREIGN KEY (request_type) REFERENCES request_type (type)
);
