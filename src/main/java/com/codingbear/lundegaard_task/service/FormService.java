package com.codingbear.lundegaard_task.service;

import com.codingbear.lundegaard_task.entity.Form;
import com.codingbear.lundegaard_task.repository.FormRepository;
import com.codingbear.lundegaard_task.resource.converter.FormConverter;
import com.codingbear.lundegaard_task.resource.entity.FormEntity;
import org.springframework.stereotype.Service;

/**
 * Business logic related to {@link Form} which can be invoked by REST API.
 */
@Service
public class FormService {

    private final FormRepository formRepository;

    public FormService(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    /**
     * Saves a new form.
     *
     * @param formEntity REST entity representing a new form
     * @return REST entity representing the newly created form
     */
    public FormEntity createForm(FormEntity formEntity) {
        Form form = FormConverter.fromEntity(formEntity);
        Form savedForm = formRepository.save(form);
        return FormConverter.toEntity(savedForm);
    }
}
