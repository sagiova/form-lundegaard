package com.codingbear.lundegaard_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring Boot class for the 'online' part of the application.
 */
@SpringBootApplication
public class LundegaardTaskApplication {

    /**
     * An entry point for the application.
     *
     * @param args application arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(LundegaardTaskApplication.class, args);
    }
}
