package com.codingbear.lundegaard_task.resource;
import com.codingbear.lundegaard_task.resource.entity.FormEntity;
import com.codingbear.lundegaard_task.service.FormService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A resource for accepting contact forms from FE.
 */
@RestController
@RequestMapping(path = "/v1/contact-forms", produces = MediaType.APPLICATION_JSON_VALUE)
public class FormResource {

    private final FormService formService;

    public FormResource(FormService formService) {
        this.formService = formService;
    }

    /**
     * Creates a new form according to the provided data.
     *
     * @return the newly created form
     */
    @PostMapping
    public FormEntity createForm(@RequestBody final FormEntity formEntity) {
        return formService.createForm(formEntity);
    }
}
