package com.codingbear.lundegaard_task.resource.converter;
import com.codingbear.lundegaard_task.entity.Form;
import com.codingbear.lundegaard_task.resource.entity.FormEntity;

/**
 * A collection of methods for conversions between the internal representation of a {@link Form}
 * and it's REST entity {@link FormEntity}.
 */
public class FormConverter {

    /**
     * Converts a REST entity into an internal representation of a request form.
     *
     * @param entity the REST entity to be converted
     * @return internal representation of a form
     */
    public static Form fromEntity(FormEntity entity) {
        Form form = new Form();
        form.setId(null);
        form.setRequestType(RequestTypeConverter.fromEntity(entity.getRequestType()));
        form.setEmail(entity.getEmail());
        form.setPolicyNumber(entity.getPolicyNumber());
        form.setName(entity.getName());
        form.setSurname(entity.getSurname());
        form.setText(entity.getText());
        return form;
    }

    /**
     * Converts an internal representation of a request form into a REST entity.
     *
     * @param form the form to be converted
     * @return the REST representation
     */
    public static FormEntity toEntity(Form form) {
        FormEntity entity = new FormEntity();
        entity.setId(form.getId());
        entity.setRequestType(RequestTypeConverter.toEntity(form.getRequestType()));
        entity.setEmail(form.getEmail());
        entity.setPolicyNumber(form.getPolicyNumber());
        entity.setName(form.getName());
        entity.setSurname(form.getSurname());
        entity.setText(form.getText());
        return entity;
    }
}
