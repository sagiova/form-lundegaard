package com.codingbear.lundegaard_task.resource.converter;
import com.codingbear.lundegaard_task.entity.RequestType;

import java.util.Arrays;

/**
 * A collection of methods for conversions between the internal representation of a {@link RequestType}
 * and a string used to represent it externally.
 */
public class RequestTypeConverter {

    /**
     * Converts a single internal representation of a request type into a REST representation.
     *
     * @param requestType the requestType to be converted
     * @return the string representation
     */
    public static String toEntity(RequestType requestType) {
        return requestType.getLabel();
    }

    /**
     * Converts a single REST representation of a request type into an internal entity.
     *
     * @param entity the string representation
     * @return the requestType
     */
    public static RequestType fromEntity(String entity) {
        return Arrays.stream(RequestType.values())
                .filter(requestType -> requestType.getLabel().equals(entity))
                .findFirst()
                .orElse(null);
    }
}
