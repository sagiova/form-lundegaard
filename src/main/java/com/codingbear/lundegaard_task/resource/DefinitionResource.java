package com.codingbear.lundegaard_task.resource;

import com.codingbear.lundegaard_task.entity.RequestType;
import com.codingbear.lundegaard_task.resource.converter.RequestTypeConverter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A resource for accessing data statically defined in the system.
 */
@RestController
@RequestMapping(path = "/v1/definitions", produces = MediaType.APPLICATION_JSON_VALUE)
public class DefinitionResource {

    /**
     * Lists all request types which are defined in the system.
     *
     * @return a list of all request types
     */
    @GetMapping("/request-types")
    public List<String> getAllRequestTypes() {
        return Arrays.stream(RequestType.values())
                .map(RequestTypeConverter::toEntity)
                .sorted()
                .collect(Collectors.toList());
    }
}
