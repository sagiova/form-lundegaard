package com.codingbear.lundegaard_task.resource.entity;

/**
 * REST entity representing a form.
 */
public class FormEntity {

    /**
     * Unique identifier of the form.
     */
    private Integer id;

    /**
     * Type of this request form that the customer chooses.
     */
    private String requestType;

    /**
     * Email of the customer sending the form.
     */
    private String email;

    /**
     * Policy number of the customer sending the form.
     */
    private String policyNumber;

    /**
     * First name of the customer sending the form.
     */
    private String name;

    /**
     * Surname of the customer sending the form.
     */
    private String surname;

    /**
     * Text of the request.
     */
    private String text;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
