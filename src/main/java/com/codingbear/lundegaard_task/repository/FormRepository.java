package com.codingbear.lundegaard_task.repository;

import com.codingbear.lundegaard_task.entity.Form;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * A collection of methods for storing and querying {@link Form} to/from the database.
 */
@Repository
public interface FormRepository extends JpaRepository<Form, Integer> {

}
