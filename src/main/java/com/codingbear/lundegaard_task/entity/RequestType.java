package com.codingbear.lundegaard_task.entity;

/**
 * Types of requests that the customers can choose from.
 */
public enum RequestType {

    /**
     * Adjusting existing contract.
     */
    CONTRACT_ADJUSTMENT("Contract adjustment"),

    /**
     * Reporting a damage case.
     */
    DAMAGE_CASE("Damage case"),

    /**
     * Sending a complaint.
     */
    COMPLAINT("Complaint");

    private final String label;

    RequestType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
