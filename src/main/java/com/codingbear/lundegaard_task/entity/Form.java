package com.codingbear.lundegaard_task.entity;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * A database entity representing a contact form.
 */
@Entity
public class Form {

    /**
     * Unique identifier of the form.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Type of this request form that the customer chooses.
     */
    @Enumerated(EnumType.STRING)
    private RequestType requestType;

    /**
     * Email of the customer sending the form.
     */
    private String email;

    /**
     * Policy number of the customer sending the form.
     */
    private String policyNumber;

    /**
     * First name of the customer sending the form.
     */
    private String name;

    /**
     * Surname of the customer sending the form.
     */
    private String surname;

    /**
     * Text of the request.
     */
    private String text;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
